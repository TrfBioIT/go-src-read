// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package syntax

// 用无符号整数表示Token
type token uint

//go:generate stringer -type token -linecomment tokens.go
// Token分为4大类：标识符及字面值、操作符、分隔符、关键字
const (
	_    token = iota
	_EOF       // EOF

	// names and literals
	_Name    // name
	_Literal // literal

	// operators and operations
	// _Operator is excluding '*' (_Star)
	_Operator // op 多于一个字符的操作符都是要特殊处理的
	_AssignOp // op= 多于一个字符的操作符都是要特殊处理的
	_IncOp    // opop 多于一个字符的操作符都是要特殊处理的
	_Assign   // =
	_Define   // := 多于一个字符的操作符都是要特殊处理的。看代码时可以看到，当遇到:时，后面要紧跟着判断是不是=
	_Arrow    // <- 多于一个字符的操作符都是要特殊处理的
	_Star     // *

	// delimiters
	_Lparen    // (
	_Lbrack    // [
	_Lbrace    // {
	_Rparen    // )
	_Rbrack    // ]
	_Rbrace    // }
	_Comma     // ,
	_Semi      // ;
	_Colon     // :
	_Dot       // .
	_DotDotDot // ... 多于一个字符的分隔符都是要特殊处理的

	// keywords 可以看出Go1.18有25个关键字
	_Break       // break
	_Case        // case
	_Chan        // chan
	_Const       // const
	_Continue    // continue
	_Default     // default
	_Defer       // defer
	_Else        // else
	_Fallthrough // fallthrough
	_For         // for
	_Func        // func
	_Go          // go
	_Goto        // goto
	_If          // if
	_Import      // import
	_Interface   // interface
	_Map         // map
	_Package     // package
	_Range       // range
	_Return      // return
	_Select      // select
	_Struct      // struct
	_Switch      // switch
	_Type        // type
	_Var         // var

	// empty line comment to exclude it from .String
	tokenCount //
)

const (
	// for BranchStmt
	Break       = _Break
	Continue    = _Continue
	Fallthrough = _Fallthrough
	Goto        = _Goto

	// for CallStmt
	Go    = _Go
	Defer = _Defer
)

// Make sure we have at most 64 tokens so we can use them in a set.
// 查看是否能用64位无符号int每个位代表一个token，所以tokens不能超过64
const _ uint64 = 1 << (tokenCount - 1)

// contains reports whether tok is in tokset.
// 这个设计很巧妙，通过位运算查看某个token是否在tokset中
func contains(tokset uint64, tok token) bool {
	return tokset&(1<<tok) != 0
}

type LitKind uint8

// TODO(gri) With the 'i' (imaginary) suffix now permitted on integer
//           and floating-point numbers, having a single ImagLit does
//           not represent the literal kind well anymore. Remove it?
const (
	IntLit LitKind = iota
	FloatLit
	ImagLit
	RuneLit
	StringLit
)

type Operator uint

//go:generate stringer -type Operator -linecomment tokens.go
// 可以看出把操作符分优先级了，precMul优先级最大。优先级在语法分析分析表达式的时候，要处理
// 同样的，只要是多个字符的，在实际处理中都要特殊处理，当出现第一个字符，要继续探索。满足则识别为
// 多字符的Token。失败会有回溯逻辑，识别为短Token
const (
	_ Operator = iota

	// Def is the : in :=
	Def   // :
	Not   // !
	Recv  // <-
	Tilde // ~

	// precOrOr
	OrOr // ||

	// precAndAnd
	AndAnd // &&

	// precCmp
	Eql // ==
	Neq // !=
	Lss // <
	Leq // <=
	Gtr // >
	Geq // >=

	// precAdd
	Add // +
	Sub // -
	Or  // |
	Xor // ^

	// precMul
	Mul    // *
	Div    // /
	Rem    // %
	And    // &
	AndNot // &^
	Shl    // <<
	Shr    // >>
)

// Operator precedences
const (
	_ = iota
	precOrOr
	precAndAnd
	precCmp
	precAdd
	precMul
)
