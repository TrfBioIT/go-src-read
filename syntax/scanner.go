// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file implements scanner, a lexical tokenizer for
// Go source. After initialization, consecutive calls of
// next advance the scanner one token at a time.
//
// This file, source.go, tokens.go, and token_string.go are self-contained
// (`go tool compile scanner.go source.go tokens.go token_string.go` compiles)
// and thus could be made into their own package.

package syntax

import (
	"fmt"
	"io"
	"unicode"
	"unicode/utf8"
)

// The mode flags below control which comments are reported
// by calling the error handler. If no flag is set, comments
// are ignored.
const (
	comments   uint = 1 << iota // call handler for all comments
	directives                  // call handler for directives only
)

type scanner struct {
	source // source.go中定义的结构体，能够帮助基于缓存读取源码，维护如行列等读取信息
	mode   uint
	// 当nlsemi为true时，遇到换行转换为; token
	// 即有些Token后面必须跟着;，而go;可以省略不写，这时候就要判断有无换行了
	nlsemi bool // if set '\n' and EOF translate to ';'
	// current token, valid after calling next()
	// 注意区分source.go中的行列。这是保存每个token开头的行列
	line, col uint
	blank     bool // line is blank up to col
	// 获取的当前token
	tok token
	// _Literal对应原始值千变万化，需要记录。
	// _Semi记录的原因是，它可能是换行或者EOF变来的
	lit string // valid if tok is _Name, _Literal, or _Semi ("semicolon", "newline", or "EOF"); may be malformed if bad is true
	bad bool   // valid if tok is _Literal, true if a syntax error occurred, lit may be malformed
	// 字面量有很多种，kind就是用来标识字面量种类的
	kind LitKind // valid if tok is _Literal
	// 最后2个字段是配合使用的，_Operator对应的操作符有多种。要配合op来区分
	op Operator // valid if tok is _Operator, _AssignOp, or _IncOp
	// 记录操作符优先级
	prec int // valid if tok is _Operator, _AssignOp, or _IncOp
}

// init由语法分析器调用。同样的，核心函数也是在语法分析时实时调用。而不是词法分析完全完成后再语法分析
func (s *scanner) init(src io.Reader, errh func(line, col uint, msg string), mode uint) {
	s.source.init(src, errh)
	s.mode = mode
	s.nlsemi = false
}

// errorf reports an error at the most recently read character position.
// 调用source中的error。即报错的位置是实时位置（出错的char的前一个位置）
func (s *scanner) errorf(format string, args ...interface{}) {
	s.error(fmt.Sprintf(format, args...))
}

// errorAtf reports an error at a byte column offset relative to the current token start.
// 报错位置是扫描的当前token的开头
func (s *scanner) errorAtf(offset int, format string, args ...interface{}) {
	s.errh(s.line, s.col+uint(offset), fmt.Sprintf(format, args...))
}

// setLit sets the scanner state for a recognized _Literal token.
func (s *scanner) setLit(kind LitKind, ok bool) {
	s.nlsemi = true // 可以看到字面量nlsemi也是true
	s.tok = _Literal
	s.lit = string(s.segment())
	s.bad = !ok
	s.kind = kind
}

// next advances the scanner by reading the next token.
//
// If a read, source encoding, or lexical error occurs, next calls
// the installed error handler with the respective error position
// and message. The error message is guaranteed to be non-empty and
// never starts with a '/'. The error handler must exist.
//
// If the scanner mode includes the comments flag and a comment
// (including comments containing directives) is encountered, the
// error handler is also called with each comment position and text
// (including opening /* or // and closing */, but without a newline
// at the end of line comments). Comment text always starts with a /
// which can be used to distinguish these handler calls from errors.
//
// If the scanner mode includes the directives (but not the comments)
// flag, only comments containing a //line, /*line, or //go: directive
// are reported, in the same way as regular comments.
// next()是词法分析核心函数，它每次读取一个Token
func (s *scanner) next() {
	nlsemi := s.nlsemi
	s.nlsemi = false // 保留上一个token需要的nlsemi状态后重置

redo:
	// skip white space 使用循环跳过空白换行等字符
	// 可以看出换行符只有上一个token不要求nlsemi(换行转变为;)时才跳过换行
	s.stop() // 每次读取Token的时候重置s.b。注意判别的是，s.r不是也一起变了。s.b会通过start函数开启新的segment的时候。移动到s.r附近
	startLine, startCol := s.pos() // 保存最开始时执行next时的行列位置
	for s.ch == ' ' || s.ch == '\t' || s.ch == '\n' && !nlsemi || s.ch == '\r' {
		s.nextch()
	}

	// token start
	s.line, s.col = s.pos() // token的开始行列
	s.blank = s.line > startLine || startCol == colbase
	s.start() // 开启新的segment，即相当于开始获取一个token
	if isLetter(s.ch) || s.ch >= utf8.RuneSelf && s.atIdentChar(true) {
		s.nextch()
		s.ident()
		return
	}
	// 处理非标识符,关键字 token
	switch s.ch {
	case -1:
		if nlsemi {
			s.lit = "EOF"
			s.tok = _Semi
			break
		}
		s.tok = _EOF

	case '\n': // 这是不忽略的换行
		s.nextch()
		s.lit = "newline"
		s.tok = _Semi

	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		s.number(false)

	case '"':
		s.stdString()

	case '`':
		s.rawString()

	case '\'':
		s.rune()

	case '(':
		s.nextch()
		s.tok = _Lparen

	case '[':
		s.nextch()
		s.tok = _Lbrack

	case '{':
		s.nextch()
		s.tok = _Lbrace

	case ',':
		s.nextch()
		s.tok = _Comma

	case ';':
		s.nextch()
		s.lit = "semicolon"
		s.tok = _Semi

	case ')':
		s.nextch()
		s.nlsemi = true // ) } ]三者需要nlsemi
		s.tok = _Rparen

	case ']':
		s.nextch()
		s.nlsemi = true
		s.tok = _Rbrack

	case '}':
		s.nextch()
		s.nlsemi = true
		s.tok = _Rbrace

	case ':':
		s.nextch()
		if s.ch == '=' { // := 单独处理
			s.nextch()
			s.tok = _Define
			break
		}
		s.tok = _Colon

	case '.':
		s.nextch()
		if isDecimal(s.ch) { // 无整数小数分支
			s.number(true)
			break
		}
		if s.ch == '.' {
			s.nextch()
			if s.ch == '.' { // ...分支
				s.nextch()
				s.tok = _DotDotDot
				break
			}
			// rewind的作用是回溯，即回到第一个.的前面
			s.rewind() // now s.ch holds 1st '.'
			// 再次吞掉第一个.
			s.nextch() // consume 1st '.' again
		}
		s.tok = _Dot // 单独的.

	case '+':
		s.nextch()
		s.op, s.prec = Add, precAdd
		if s.ch != '+' { // 这里去尝试匹配 assignop  + or +=
			goto assignop
		}
		// 这里说明是2个+
		s.nextch()
		s.nlsemi = true // 可以看出++和--也设置了nlsemi
		s.tok = _IncOp  // ++

	case '-':
		s.nextch()
		s.op, s.prec = Sub, precAdd
		if s.ch != '-' { // - or -=
			goto assignop
		}
		s.nextch()
		s.nlsemi = true
		s.tok = _IncOp // --

	case '*':
		s.nextch()
		s.op, s.prec = Mul, precMul
		// don't goto assignop - want _Star token
		if s.ch == '=' { // *=
			s.nextch()
			s.tok = _AssignOp
			break
		}
		s.tok = _Star // * 不能去assignop分支，因为单独的*还有可能是取值符或者代表指针

	case '/': // 比较特殊，因为可以是注释
		s.nextch()
		if s.ch == '/' {
			s.nextch()
			s.lineComment()
			goto redo // 注释处理完要读取一个新的Token返回
		}
		if s.ch == '*' {
			s.nextch()
			s.fullComment()
			// 注意下面的line是当前行。s.line是next函数开头时记录的token开始行
			if line, _ := s.pos(); line > s.line && nlsemi {
				// A multi-line comment acts like a newline;
				// it translates to a ';' if nlsemi is set.
				// 由于多行注释跨越了多行可以吞掉或者连接的代码没有换行，所以需要它充当换行
				s.lit = "newline"
				s.tok = _Semi
				break
			}
			goto redo
		}
		s.op, s.prec = Div, precMul
		goto assignop // / or /=

	case '%':
		s.nextch()
		s.op, s.prec = Rem, precMul
		goto assignop // % or %=

	case '&':
		s.nextch()
		if s.ch == '&' { // &&
			s.nextch()
			s.op, s.prec = AndAnd, precAndAnd
			s.tok = _Operator
			break
		}
		s.op, s.prec = And, precMul
		if s.ch == '^' { // &^
			s.nextch()
			s.op = AndNot
		}
		goto assignop // & or &=

	case '|':
		s.nextch()
		if s.ch == '|' { // ||
			s.nextch()
			s.op, s.prec = OrOr, precOrOr
			s.tok = _Operator
			break
		}
		s.op, s.prec = Or, precAdd
		goto assignop // |  or |=

	case '^':
		s.nextch()
		s.op, s.prec = Xor, precAdd
		goto assignop // ^ or ^=

	case '<':
		s.nextch()
		if s.ch == '=' { // <=
			s.nextch()
			s.op, s.prec = Leq, precCmp
			s.tok = _Operator
			break
		}
		if s.ch == '<' { // << or <<=
			s.nextch()
			s.op, s.prec = Shl, precMul
			goto assignop
		}
		if s.ch == '-' { // <- 注意箭头在词法分析阶段都是属于_Arrow token。而没有接收操作符。类似乘法
			s.nextch()
			s.tok = _Arrow
			break
		}
		s.op, s.prec = Lss, precCmp // <
		s.tok = _Operator

	case '>':
		s.nextch()
		if s.ch == '=' { // >=
			s.nextch()
			s.op, s.prec = Geq, precCmp
			s.tok = _Operator
			break
		}
		if s.ch == '>' { // >> or >>=
			s.nextch()
			s.op, s.prec = Shr, precMul
			goto assignop
		}
		s.op, s.prec = Gtr, precCmp
		s.tok = _Operator // >

	case '=':
		s.nextch()
		if s.ch == '=' { // ==
			s.nextch()
			s.op, s.prec = Eql, precCmp
			s.tok = _Operator
			break
		}
		s.tok = _Assign // =

	case '!':
		s.nextch()
		if s.ch == '=' { // !=
			s.nextch()
			s.op, s.prec = Neq, precCmp
			s.tok = _Operator
			break
		}
		s.op, s.prec = Not, 0
		s.tok = _Operator // !

	case '~': // go最后加入的操作符
		s.nextch()
		s.op, s.prec = Tilde, 0
		s.tok = _Operator

	default: // 出错报错，然后继续读取下一个token
		s.errorf("invalid character %#U", s.ch)
		s.nextch()
		goto redo
	}

	return

assignop:
	if s.ch == '=' { // op=格式为赋值运算符
		s.nextch()
		s.tok = _AssignOp
		return
	}
	s.tok = _Operator
}

func (s *scanner) ident() {
	// 1. 一直读取完规则内的字符
	// accelerate common case (7bit ASCII)
	for isLetter(s.ch) || isDecimal(s.ch) {
		s.nextch()
	}
	// general case 非ASCII字符
	if s.ch >= utf8.RuneSelf {
		for s.atIdentChar(false) {
			s.nextch()
		}
	}
	// 2.区分标识符或者关键字分别处理
	// possibly a keyword
	lit := s.segment()
	if len(lit) >= 2 {
		if tok := keywordMap[hash(lit)]; tok != 0 && tokStrFast(tok) == string(lit) {
			// 可以看出以下几个可以看做跳转的语句需要设置nlsemi
			s.nlsemi = contains(1<<_Break|1<<_Continue|1<<_Fallthrough|1<<_Return, tok)
			s.tok = tok
			return
		}
	}
	// 标识符需要设置nlsemi
	s.nlsemi = true
	s.lit = string(lit) // 标识符记录表示符原始
	s.tok = _Name
}

// tokStrFast is a faster version of token.String, which assumes that tok
// is one of the valid tokens - and can thus skip bounds checks.
func tokStrFast(tok token) string {
	return _token_name[_token_index[tok-1]:_token_index[tok]]
}

func (s *scanner) atIdentChar(first bool) bool {
	switch {
	case unicode.IsLetter(s.ch) || s.ch == '_': // 这里的IsLetter不是英文字母的小写字母。所以标识符支持很多开头
		// ok
	case unicode.IsDigit(s.ch): // 标识符不能以数字开头
		if first {
			s.errorf("identifier cannot begin with digit %#U", s.ch)
		}
	case s.ch >= utf8.RuneSelf:
		s.errorf("invalid character %#U in identifier", s.ch)
	default:
		return false
	}
	return true
}

// hash is a perfect hash function for keywords.
// It assumes that s has at least length 2.
// hash函数把关键字变成了一个uint值。所有关键字uint值不一样(要避免冲突)
func hash(s []byte) uint {
	return (uint(s[0])<<4 ^ uint(s[1]) + uint(len(s))) & uint(len(keywordMap)-1)
}

// keywordMap相当于一个map
var keywordMap [1 << 6]token // size must be power of two
// 初始化函数将所有关键字Token放入了一个临时实现的set中
func init() {
	// populate keywordMap
	for tok := _Break; tok <= _Var; tok++ {
		h := hash([]byte(tok.String()))
		if keywordMap[h] != 0 {
			panic("imperfect hash")
		}
		keywordMap[h] = tok
	}
}

func lower(ch rune) rune     { return ('a' - 'A') | ch } // returns lower-case ch iff ch is ASCII letter
func isLetter(ch rune) bool  { return 'a' <= lower(ch) && lower(ch) <= 'z' || ch == '_' }
func isDecimal(ch rune) bool { return '0' <= ch && ch <= '9' }
func isHex(ch rune) bool     { return '0' <= ch && ch <= '9' || 'a' <= lower(ch) && lower(ch) <= 'f' }

// digits accepts the sequence { digit | '_' }.
// If base <= 10, digits accepts any decimal digit but records
// the index (relative to the literal start) of a digit >= base
// in *invalid, if *invalid < 0.
// digits returns a bitset describing whether the sequence contained
// digits (bit 0 is set), or separators '_' (bit 1 is set).
// digits的作用有2大点:
// 1. 检测<= 10进制是否含非当前进制的非法数字（不是非法字符）
// 2. 检测数字串是否含数字和_
// 一样的，不管什么进制，遇到了非法字符都会停止扫描。<= 10进制的非法字符是
func (s *scanner) digits(base int, invalid *int) (digsep int) {
	if base <= 10 {
		max := rune('0' + base) // 每一位不能超过这个数
		for isDecimal(s.ch) || s.ch == '_' {
			ds := 1 // 为数字时
			if s.ch == '_' {
				ds = 2
			} else if s.ch >= max && *invalid < 0 { // 记录第一次出现违规数字
				_, col := s.pos()
				*invalid = int(col - s.col) // record invalid rune index
			}
			digsep |= ds
			s.nextch()
		}
	} else { // 16进制单独处理
		for isHex(s.ch) || s.ch == '_' {
			ds := 1
			if s.ch == '_' {
				ds = 2
			}
			digsep |= ds
			s.nextch()
		}
	}
	return
}

func (s *scanner) number(seenPoint bool) {
	ok := true
	kind := IntLit
	base := 10        // number base
	prefix := rune(0) // one of 0 (decimal), '0' (0-octal), 'x', 'o', or 'b'
	digsep := 0       // bit 0: digit present, bit 1: '_' present
	invalid := -1     // index of invalid digit in literal, or < 0

	// integer part
	if !seenPoint {
		// 非10进制 处理base, prefix, digsep,  invalid
		if s.ch == '0' {
			s.nextch()
			switch lower(s.ch) { // 位运算把大小写统一化为小写
			case 'x':
				s.nextch()
				base, prefix = 16, 'x'
			case 'o':
				s.nextch()
				base, prefix = 8, 'o'
			case 'b':
				s.nextch()
				base, prefix = 2, 'b'
			default:
				base, prefix = 8, '0'
				// 注意一下，这里我们没有s.nextch()。所以调用digits方法时这个分支还是没变的
				// 同时digsep标为1是提前预测了一下这个位置是数字
				digsep = 1 // leading 0
			}
		}
		// digsep就是数字中有无数字和_的。当然，如果我们是10进制，这里还是我们最初进赖number时的数字
		digsep |= s.digits(base, &invalid)
		// 如果我们经过digits的遍历，是因为当前字符是"."而停止的，处理相关逻辑
		if s.ch == '.' {
			if prefix == 'o' || prefix == 'b' { // 可以看出0o格式8进制和0b格式二进制不能有小数。其实如果是0开头八进制，此时就看做10进制了
				s.errorf("invalid radix point in %s literal", baseName(base))
				ok = false
			}
			s.nextch()
			seenPoint = true // 说明有小数点，让后面的小数点处理分支生效
		}
	}

	// fractional part 处理小数部分
	if seenPoint {
		kind = FloatLit
		digsep |= s.digits(base, &invalid)
	}
	// 没有数字。可以看出只要支持浮点数的进制。其小数点前后只要有一个有数字就行
	if digsep&1 == 0 && ok {
		s.errorf("%s literal has no digits", baseName(base))
		ok = false
	}

	// exponent
	// 处理指数
	if e := lower(s.ch); e == 'e' || e == 'p' {
		if ok { // 之前没出过错才检查有没有错
			switch {
			case e == 'e' && prefix != 0 && prefix != '0': // 可以看出只有0开头8进制和10进制允许指数，其实已经不是八进制了
				s.errorf("%q exponent requires decimal mantissa", s.ch)
				ok = false
			case e == 'p' && prefix != 'x': // p和16进制双向绑定
				s.errorf("%q exponent requires hexadecimal mantissa", s.ch)
				ok = false
			}
		}
		s.nextch() // 吞掉e或者p
		kind = FloatLit
		if s.ch == '+' || s.ch == '-' { // 吞掉e/p后的正负表示
			s.nextch()
		}
		// 吞掉指数后的数字，并检查是否有无数字错误。
		digsep = s.digits(10, nil) | digsep&2 // don't lose sep bit
		if digsep&1 == 0 && ok {              // 没有数字错误
			s.errorf("exponent has no digits")
			ok = false
		}
	} else if prefix == 'x' && kind == FloatLit && ok { // 有小数点的16进制必须有p。反之不需要，即单独只有指数是可以的
		s.errorf("hexadecimal mantissa requires a 'p' exponent")
		ok = false
	}

	// suffix 'i'  即虚数处理
	if s.ch == 'i' {
		kind = ImagLit
		s.nextch()
	}
	// 到这里，数字的形式即有无错误已经排查完了。记录数字kind。及是否出错
	s.setLit(kind, ok) // do this now so we can use s.lit below
	// 还记得上面吗，上面有非法数字时只是用invalid记录了，这里加上处理
	// kind == IntLit的原因是，如果是FloatLit。二进制和0o开头八进制已经有其它报错了。0版本八进制此时此刻在这里其实已经10进制了，那么也没有报错的必要
	// 但是如果是IntLit的0开头8进制，那么是要报错的。这就是为什么09报错，099.1不报错的原因
	if kind == IntLit && invalid >= 0 && ok {
		s.errorAtf(invalid, "invalid digit %q in %s literal", s.lit[invalid], baseName(base))
		ok = false
	}
	// 检查是否有不规范分割
	if digsep&2 != 0 && ok {
		if i := invalidSep(s.lit); i >= 0 {
			s.errorAtf(i, "'_' must separate successive digits")
			ok = false
		}
	}
	// 由于上面2步可能引入新错误，这里纠正
	s.bad = !ok // correct s.bad
}

func baseName(base int) string {
	switch base {
	case 2:
		return "binary"
	case 8:
		return "octal"
	case 10:
		return "decimal"
	case 16:
		return "hexadecimal"
	}
	panic("invalid base")
}

// invalidSep returns the index of the first invalid separator in x, or -1.
func invalidSep(x string) int {
	x1 := ' ' // prefix char, we only care if it's 'x'
	d := '.'  // digit, one of '_', '0' (a digit), or '.' (anything else)
	i := 0

	// a prefix counts as a digit
	if len(x) >= 2 && x[0] == '0' {
		x1 = lower(rune(x[1]))
		if x1 == 'x' || x1 == 'o' || x1 == 'b' {
			d = '0'
			i = 2
		}
	}

	// mantissa and exponent
	for ; i < len(x); i++ {
		p := d // previous digit
		d = rune(x[i])
		switch {
		case d == '_':
			if p != '0' {
				return i
			}
		case isDecimal(d) || x1 == 'x' && isHex(d):
			d = '0'
		default:
			if p == '_' {
				return i - 1
			}
			d = '.'
		}
	}
	if d == '_' {
		return len(x) - 1
	}

	return -1
}

// rune() 处理字符字面量
func (s *scanner) rune() {
	ok := true
	s.nextch()

	n := 0 // 字符长度只能为1
	for ; ; n++ {
		if s.ch == '\'' { // 遇到结尾，检查字符长度是否为1
			if ok {
				if n == 0 {
					s.errorf("empty rune literal or unescaped '")
					ok = false
				} else if n != 1 {
					s.errorAtf(0, "more than one character in rune literal")
					ok = false
				}
			}
			s.nextch()
			break
		}
		if s.ch == '\\' { // 转义，检查转义是否合规
			s.nextch()
			if !s.escape('\'') {
				ok = false
			}
			continue
		}
		if s.ch == '\n' { // 真实换行不允许
			if ok {
				s.errorf("newline in rune literal")
				ok = false
			}
			break
		}
		if s.ch < 0 {
			if ok {
				s.errorAtf(0, "rune literal not terminated")
				ok = false
			}
			break
		}
		s.nextch()
	}

	s.setLit(RuneLit, ok)
}

// stdString 扫描出一个字符串字面量
func (s *scanner) stdString() {
	ok := true
	s.nextch() // 吞掉"

	for {
		if s.ch == '"' { // 字符串结束
			s.nextch()
			break
		}
		// 我们从原文件中读的字符就是一个个字符，转义是语言规定的，不是从编码就规定的
		// 所以源文件中\n是2个字符
		if s.ch == '\\' { // 转义，检查\\后的字符是否正确
			s.nextch()
			if !s.escape('"') {
				ok = false
			}
			continue
		}
		if s.ch == '\n' { // 注意，这里是真换行，不是转义形成的换行
			s.errorf("newline in string")
			ok = false
			break
		}
		if s.ch < 0 {
			s.errorAtf(0, "string not terminated")
			ok = false
			break
		}
		s.nextch()
	}

	s.setLit(StringLit, ok)
}

// rawString。其实``里面的字符串就相当于从文件中读取的，转义失效
func (s *scanner) rawString() {
	ok := true
	s.nextch()

	for {
		if s.ch == '`' {
			s.nextch()
			break
		}
		if s.ch < 0 {
			s.errorAtf(0, "string not terminated")
			ok = false
			break
		}
		s.nextch()
	}
	// We leave CRs in the string since they are part of the
	// literal (even though they are not part of the literal
	// value).

	s.setLit(StringLit, ok)
}

func (s *scanner) comment(text string) {
	s.errorAtf(0, "%s", text)
}

// skipLine 吞掉一行的所有字符。但是保留换行
func (s *scanner) skipLine() {
	// don't consume '\n' - needed for nlsemi logic
	// 这里可以看出，换行符不能轻易消费，因为上一个token可能需要nlsemi
	for s.ch >= 0 && s.ch != '\n' {
		s.nextch()
	}
}

func (s *scanner) lineComment() {
	// opening has already been consumed

	// 所有注释都要处理
	if s.mode&comments != 0 {
		s.skipLine()
		s.comment(string(s.segment()))
		return
	}

	// are we saving directives? or is this definitely not a directive?
	// 没有开启处理directives。或者是普通注解。跳过注释行
	if s.mode&directives == 0 || (s.ch != 'g' && s.ch != 'l') {
		s.stop()
		s.skipLine()
		return
	}

	// recognize go: or line directives
	prefix := "go:"
	if s.ch == 'l' {
		prefix = "line "
	}
	for _, m := range prefix { // 识别是不是go:或行指令特殊注释
		if s.ch != m {
			s.stop()
			s.skipLine()
			return
		}
		s.nextch()
	}

	// directive text
	// 可以看出 directive text 和错误信息一样处理。
	s.skipLine()
	s.comment(string(s.segment()))
}

func (s *scanner) skipComment() bool {
	for s.ch >= 0 {
		for s.ch == '*' {
			s.nextch()
			if s.ch == '/' {
				s.nextch()
				return true
			}
		}
		s.nextch()
	}
	s.errorAtf(0, "comment not terminated")
	return false
}

func (s *scanner) fullComment() {
	/* opening has already been consumed */

	if s.mode&comments != 0 {
		if s.skipComment() {
			s.comment(string(s.segment()))
		}
		return
	}

	if s.mode&directives == 0 || s.ch != 'l' {
		s.stop()
		s.skipComment()
		return
	}

	// recognize line directive
	const prefix = "line "
	for _, m := range prefix {
		if s.ch != m {
			s.stop()
			s.skipComment()
			return
		}
		s.nextch()
	}

	// directive text
	if s.skipComment() {
		s.comment(string(s.segment()))
	}
}

// 复用的检查是否是正确转义。quote在",'等里面传不一样的就行了
func (s *scanner) escape(quote rune) bool {
	var n int
	var base, max uint32

	switch s.ch {
	case quote, 'a', 'b', 'f', 'n', 'r', 't', 'v', '\\':
		s.nextch()
		return true
	case '0', '1', '2', '3', '4', '5', '6', '7':
		n, base, max = 3, 8, 255
	case 'x':
		s.nextch()
		n, base, max = 2, 16, 255
	case 'u':
		s.nextch()
		n, base, max = 4, 16, unicode.MaxRune
	case 'U':
		s.nextch()
		n, base, max = 8, 16, unicode.MaxRune
	default:
		if s.ch < 0 {
			return true // complain in caller about EOF
		}
		s.errorf("unknown escape")
		return false
	}

	var x uint32
	for i := n; i > 0; i-- {
		if s.ch < 0 {
			return true // complain in caller about EOF
		}
		d := base
		if isDecimal(s.ch) {
			d = uint32(s.ch) - '0'
		} else if 'a' <= lower(s.ch) && lower(s.ch) <= 'f' {
			d = uint32(lower(s.ch)) - 'a' + 10
		}
		if d >= base {
			s.errorf("invalid character %q in %s escape", s.ch, baseName(int(base)))
			return false
		}
		// d < base
		x = x*base + d
		s.nextch()
	}

	if x > max && base == 8 {
		s.errorf("octal escape value %d > 255", x)
		return false
	}

	if x > max || 0xD800 <= x && x < 0xE000 /* surrogate range */ {
		s.errorf("escape is invalid Unicode code point %#U", x)
		return false
	}

	return true
}
