// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package syntax

// ----------------------------------------------------------------------------
// Nodes

// Node相当于语法分析AST中结点的祖先
type Node interface {
	// Pos() returns the position associated with the node as follows:
	// 1) The position of a node representing a terminal syntax production
	//    (Name, BasicLit, etc.) is the position of the respective production
	//    in the source.
	// 2) The position of a node representing a non-terminal production
	//    (IndexExpr, IfStmt, etc.) is the position of a token uniquely
	//    associated with that production; usually the left-most one
	//    ('[' for IndexExpr, 'if' for IfStmt, etc.)
	Pos() Pos
	aNode()
}

// node是Node接口等简单实现。其实现了也是唯一实现了Pos()方法的继承者。作用为取到Node对应的Token的开始位置信息
type node struct {
	// commented out for now since not yet used
	// doc  *Comment // nil means no comment(s) attached
	pos Pos
}

func (n *node) Pos() Pos { return n.pos }
func (*node) aNode()     {}

// ----------------------------------------------------------------------------
// Files

// package PkgName; DeclList[0], DeclList[1], ...
type File struct {
	Pragma   Pragma
	PkgName  *Name
	DeclList []Decl
	EOF      Pos
	node
}

// ----------------------------------------------------------------------------
// Declarations
// 定义接口和实现。decl是Decl的基本实现
// 有5种定义实现：import定义、常量定义、变量定义、type定义、函数/方法定义
type (
	Decl interface {
		Node
		aDecl()
	}

	//              Path
	// LocalPkgName Path
	ImportDecl struct {
		// 批量import的时候的Group
		Group  *Group // nil means not part of a group
		Pragma Pragma
		// 包别名
		LocalPkgName *Name // including "."; nil means no rename present
		// 包路径
		Path *BasicLit // Path.Bad || Path.Kind == StringLit; nil means no path
		decl
	}

	// NameList
	// NameList      = Values
	// NameList Type = Values
	ConstDecl struct {
		Group  *Group // nil means not part of a group
		Pragma Pragma
		// 常量名集合。a, b, c int = 1, 2, 3。这里就代表a b c
		NameList []*Name
		// 常量tyep。根据语法：Type和Values不是必须的。所以注释说可以为nil
		Type Expr // nil means no type
		// 常量值
		Values Expr // nil means no values
		decl
	}

	// Name Type
	// 类型声明
	TypeDecl struct {
		Group  *Group // nil means not part of a group
		Pragma Pragma
		Name   *Name // 类型名
		// 泛型声明需要用到
		TParamList []*Field // nil means no type parameters
		Alias      bool     // 是否是别名
		Type       Expr     // 类型的类型
		decl
	}

	// NameList Type
	// NameList Type = Values
	// NameList      = Values
	VarDecl struct {
		Group    *Group // nil means not part of a group
		Pragma   Pragma
		NameList []*Name
		Type     Expr // nil means no type
		Values   Expr // nil means no values
		decl
	}

	// func          Name Type { Body }
	// func          Name Type
	// func Receiver Name Type { Body }
	// func Receiver Name Type
	FuncDecl struct {
		Pragma     Pragma
		Recv       *Field    // nil means regular function
		Name       *Name     // 函数名
		TParamList []*Field  // nil means no type parameters 泛型
		Type       *FuncType // 包含参数列表和参数返回值列表
		// 函数体，无函数体代表是预先声明
		Body *BlockStmt // nil means no body (forward declaration)
		decl
	}
)

type decl struct{ node }

func (*decl) aDecl() {}

// All declarations belonging to the same group point to the same Group node.
// 批量定义的Decl指向一个组
type Group struct {
	_ int // not empty so we are guaranteed different Group instances
}

// ----------------------------------------------------------------------------
// Expressions

func NewName(pos Pos, value string) *Name {
	n := new(Name)
	n.pos = pos
	n.Value = value
	return n
}

// 表达式接口和实现。expr是Expr的基本实现
type (
	Expr interface {
		Node
		aExpr()
	}

	// Placeholder for an expression that failed to parse
	// correctly and where we can't provide a better node.
	// 无法解析到需求Expr实现时的返回
	BadExpr struct {
		expr
	}

	// Value
	// 标识符等
	Name struct {
		Value string
		expr
	}

	// Value
	// 字面量: 包含int, float, Rune, 虚数, string
	BasicLit struct {
		Value string
		Kind  LitKind
		Bad   bool // true means the literal Value has syntax errors
		expr
	}

	// Type { ElemList[0], ElemList[1], ... }
	// 复杂表Lit
	// 1. 命名的结构体实例化：name{field1: field1Value...}，name也可以是这样：name.name
	// 2. 泛型：eg: MyMap[string, int]{key1:value1...}，这里是有type MyMap[T string, E ang] map[T]E
	// 3. 数组、切片、结构体、map实例化
	CompositeLit struct {
		Type     Expr   // nil means no literal type
		ElemList []Expr // 内容，如key-value。或者像数组初始化中的元素
		NKeys    int    // number of elements with keys
		Rbrace   Pos
		expr
	}

	// Key: Value
	// 用户结构体初始化、map元素等
	KeyValueExpr struct {
		Key, Value Expr
		expr
	}

	// func Type { Body }
	FuncLit struct {
		Type *FuncType
		Body *BlockStmt
		expr
	}

	// (X)
	// 括号括起来的表达式，也算元表达式
	ParenExpr struct {
		X Expr // 被括起来的表达式
		expr
	}

	// X.Sel
	// 有前缀的类型，如packagename.typename
	SelectorExpr struct {
		X   Expr
		Sel *Name
		expr
	}

	// X[Index]
	// X[T1, T2, ...] (with Ti = Index.(*ListExpr).ElemList[i])
	// 1. Index为ListExpr，代表T1，T2
	// 2. Index就是Index代表X[i]
	IndexExpr struct {
		X     Expr
		Index Expr
		expr
	}

	// X[Index[0] : Index[1] : Index[2]]
	// 切片表达式
	SliceExpr struct {
		X     Expr
		Index [3]Expr
		// Full indicates whether this is a simple or full slice expression.
		// In a valid AST, this is equivalent to Index[2] != nil.
		// TODO(mdempsky): This is only needed to report the "3-index
		// slice of string" error when Index[2] is missing.
		Full bool
		expr
	}

	// X.(Type)
	// 注意和TypeSwitchGuard的区别，这里的Type是确定Type
	AssertExpr struct {
		X    Expr
		Type Expr
		expr
	}

	// X.(type)
	// Lhs := X.(type)
	// 用在swich case中的类型判断
	TypeSwitchGuard struct {
		Lhs *Name // nil means no Lhs :=
		X   Expr  // X.(type)
		expr
	}

	// Operaion
	// 表示表达式
	Operation struct {
		Op   Operator
		X, Y Expr // Y == nil means unary expression
		expr
	}

	// Fun(ArgList[0], ArgList[1], ...)
	// 函数调用
	CallExpr struct {
		Fun     Expr   // 函数名
		ArgList []Expr // nil means no arguments
		// 最后一个参数是否有解压缩操作
		HasDots bool // last argument is followed by ...
		expr
	}

	// ElemList[0], ElemList[1], ...
	// 1. T[P1, P2...]中的TypeList
	// 2. 表达式列表
	ListExpr struct {
		ElemList []Expr
		expr
	}

	// [Len]Elem
	// 数组
	ArrayType struct {
		// TODO(gri) consider using Name{"..."} instead of nil (permits attaching of comments)
		Len  Expr // nil means Len is ... 数组长度
		Elem Expr // 数组元素类型
		expr
	}

	// []Elem
	SliceType struct {
		Elem Expr
		expr
	}

	// ...Elem
	DotsType struct {
		Elem Expr
		expr
	}

	// struct { FieldList[0] TagList[0]; FieldList[1] TagList[1]; ... }
	// 结构体类型，我们可以看出tags是存储在StructType结构体中的
	StructType struct {
		FieldList []*Field
		TagList   []*BasicLit // i >= len(TagList) || TagList[i] == nil means no tag for field i
		expr
	}

	// Name Type
	//      Type
	// Field可以表示很多，如函数泛型，泛型定义时，函数返回值，函数参数，接口方法，接口表示的泛型集合
	Field struct {
		Name *Name // nil means anonymous field/parameter (structs/parameters), or embedded element (interfaces)
		Type Expr  // field names declared in a list share the same Type (identical pointers)
		node
	}

	// interface { MethodList[0]; MethodList[1]; ... }
	// 接口type。可以看出由于之前不支持泛型，现在支持泛型后，起名是原来还不是特别正确
	// 如现在接口type可以为 interface{  int | float32 }。把int | float32也当成一个MethodList
	InterfaceType struct {
		MethodList []*Field
		expr
	}

	// 函数类型
	FuncType struct {
		ParamList  []*Field // 请求参数
		ResultList []*Field // 返回参数
		expr
	}

	// map[Key]Value
	// mapType记录了key和value的类型
	MapType struct {
		Key, Value Expr
		expr
	}

	//   chan Elem
	// <-chan Elem
	// chan<- Elem
	// 代表三种chanTyep：可读可写，只读，只写。如上注释所示
	ChanType struct {
		Dir  ChanDir // 0 means no direction
		Elem Expr
		expr
	}
)

type expr struct{ node }

func (*expr) aExpr() {}

// chan通道类型
type ChanDir uint

const (
	_        ChanDir = iota
	SendOnly         // chan<- Elem
	RecvOnly         // <-chan Elem
)

// ----------------------------------------------------------------------------
// Statements
// 语句接口及实现
type (
	Stmt interface {
		Node
		aStmt()
	}
	// 若干stmt的父类
	SimpleStmt interface { //  EmptyStmt | ExpressionStmt | SendStmt | IncDecStmt | Assignment | ShortVarDecl .
		Stmt
		aSimpleStmt()
	}

	EmptyStmt struct {
		simpleStmt
	}
	// 标签stmt。Stmt是标签之后的代码块，为空的话就为EmptyStmt
	LabeledStmt struct {
		Label *Name
		Stmt  Stmt
		stmt
	}
	// 最大的stmt。包含很多stmt
	BlockStmt struct {
		List   []Stmt
		Rbrace Pos
		stmt
	}

	// 没有再精确分类的表达式stmt
	ExprStmt struct {
		X Expr
		simpleStmt
	}
	// 发送值给chan的stmt
	SendStmt struct {
		Chan, Value Expr // Chan <- Value
		simpleStmt
	}
	// 不在外部的声明
	DeclStmt struct {
		DeclList []Decl
		stmt
	}

	// 赋值相关stmt
	// opop  :=  --  ++
	AssignStmt struct {
		Op       Operator // 0 means no operation
		Lhs, Rhs Expr     // Rhs == nil means Lhs++ (Op == Add) or Lhs-- (Op == Sub)
		simpleStmt
	}
	// Break, Continue, Fallthrough, or Goto语句
	BranchStmt struct {
		Tok   token // Break, Continue, Fallthrough, or Goto
		Label *Name
		// Target is the continuation of the control flow after executing
		// the branch; it is computed by the parser if CheckBranches is set.
		// Target is a *LabeledStmt for gotos, and a *SwitchStmt, *SelectStmt,
		// or *ForStmt for breaks and continues, depending on the context of
		// the branch. Target is not set for fallthroughs.
		Target Stmt
		stmt
	}
	// 解析出可以被go和defer调用的函数
	CallStmt struct {
		Tok  token     // Go or Defer
		Call *CallExpr // 函数调用表达式
		stmt
	}
	// 返回语句
	ReturnStmt struct {
		// 显式返回的列表
		Results Expr // nil means no explicit return values
		stmt
	}
	// if
	IfStmt struct {
		Init SimpleStmt // 赋值、定义并赋值等
		Cond Expr       // boolean表达式
		// if后的stmt list
		Then *BlockStmt
		// else if就是 ifstmt
		Else Stmt // either nil, *IfStmt, or *BlockStmt
		stmt
	}

	// 1. rangeClause  2. 单独一个cond 3. 其它情况必须2个;;
	ForStmt struct {
		Init SimpleStmt // incl. *RangeClause
		Cond Expr
		Post SimpleStmt
		Body *BlockStmt
		stmt
	}
	// switch
	SwitchStmt struct {
		// 定义或者赋值的
		Init SimpleStmt
		// 需要switch的
		Tag    Expr // incl. *TypeSwitchGuard
		Body   []*CaseClause
		Rbrace Pos
		stmt
	}
	// select
	SelectStmt struct {
		Body   []*CommClause
		Rbrace Pos
		stmt
	}
)

type (
	// range
	RangeClause struct {
		Lhs Expr // nil means no Lhs = or Lhs :=。即相当于循环次数是元素个数
		Def bool // means :=   区分是: 还是 :=
		X   Expr // range X
		simpleStmt
	}
	// swicth 后的 case defalut
	CaseClause struct {
		Cases Expr // nil means default clause
		Body  []Stmt
		Colon Pos // :的位置
		node
	}
	// select 后的 case defalut
	CommClause struct {
		Comm  SimpleStmt // send or receive stmt; nil means default clause
		Body  []Stmt
		Colon Pos
		node
	}
)

type stmt struct{ node }

func (stmt) aStmt() {}

type simpleStmt struct {
	stmt
}

func (simpleStmt) aSimpleStmt() {}

// ----------------------------------------------------------------------------
// Comments

// TODO(gri) Consider renaming to CommentPos, CommentPlacement, etc.
//           Kind = Above doesn't make much sense.
type CommentKind uint

const (
	Above CommentKind = iota
	Below
	Left
	Right
)

type Comment struct {
	Kind CommentKind
	Text string
	Next *Comment
}
