// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file implements source, a buffered rune reader
// specialized for scanning Go code: Reading
// ASCII characters, maintaining current (line, col)
// position information, and recording of the most
// recently read source segment are highly optimized.
// This file is self-contained (go tool compile source.go
// compiles) and thus could be made into its own package.

package syntax

import (
	"io"
	"unicode/utf8"
)

// The source buffer is accessed using three indices b (begin),
// r (read), and e (end):
//
// - If b >= 0, it points to the beginning of a segment of most
//   recently read characters (typically a Go literal).
//
// - r points to the byte immediately following the most recently
//   read character ch, which starts at r-chw.
//
// - e points to the byte immediately following the last byte that
//   was read into the buffer.
//
// The buffer content is terminated at buf[e] with the sentinel
// character utf8.RuneSelf. This makes it possible to test for
// the common case of ASCII characters with a single 'if' (see
// nextch method).
//
//                +------ content in use -------+
//                v                             v
// buf [...read...|...segment...|ch|...unread...|s|...free...]
//                ^             ^  ^            ^
//                |             |  |            |
//                b         r-chw  r            e
//
// Invariant: -1 <= b < r <= e < len(buf) && buf[e] == sentinel

type source struct {
	in    io.Reader
	errh  func(line, col uint, msg string)
	buf   []byte // source buffer
	ioerr error  // pending I/O error, or nil
	// b,r,e是缓冲区源代码的三个索引。b代表最近读取的chars的开头，r代表正在读的地方，其后是将要读的chr，e代表结束，其在读入缓冲的最后一个字节后面
	b, r, e int // buffer indices (see comment above)
	// 索引0开始的行列信息
	line, col uint // source position of ch (0-based)
	// 最近读取chr
	ch rune // most recently read character
	// chw的宽度，即r - chw == r上一次指向
	chw int // width of ch
}

const sentinel = utf8.RuneSelf

// 初始化source对象
func (s *source) init(in io.Reader, errh func(line, col uint, msg string)) {
	s.in = in
	s.errh = errh
	if s.buf == nil {
		// nextSize是分配buf大小的。这里可以看出输入0的话，初始buf大小为4k
		s.buf = make([]byte, nextSize(0))
	}
	// buf初始化后，由于读取源代码的动作是被动的。所以第一次读取时，buf不能为空，可以看到nextch中，>= sentinel
	// 的字符会去做是否继续读取原文件判断，此时被动的第一次读取源代码
	s.buf[0] = sentinel
	s.ioerr = nil
	s.b, s.r, s.e = -1, 0, 0
	s.line, s.col = 0, 0
	s.ch = ' ' // 初始ch为空格，当scanner第一次扫描时会继续读取
	s.chw = 0
}

// starting points for line and column numbers
const linebase = 1
const colbase = 1

// pos returns the (line, col) source position of s.ch.
// 就是放回最新读取的chr（1-base）的行列
func (s *source) pos() (line, col uint) {
	return linebase + s.line, colbase + s.col
}

// error reports the error msg at source position s.pos().
// 行列信息是当前行列的报错
func (s *source) error(msg string) {
	line, col := s.pos()
	s.errh(line, col, msg)
}

// start starts a new active source segment (including s.ch).
// 可以看出，最开始的时候s.b = 0，第一次开始处理源代码的时候。就是0了，我们要注意边界这些细节。且我们开启新的segment后，上一个segment是不含刚读取的chr的
// As long as stop has not been called, the active segment's
// bytes (excluding s.ch) may be retrieved by calling segment.
// segment()返回当前已经读取的segment
func (s *source) start()          { s.b = s.r - s.chw }
func (s *source) stop()           { s.b = -1 }
func (s *source) segment() []byte { return s.buf[s.b : s.r-s.chw] }

// rewind rewinds the scanner's read position and character s.ch
// to the start of the currently active segment, which must not
// contain any newlines (otherwise position information will be
// incorrect). Currently, rewind is only needed for handling the
// source sequence ".."; it must not be called outside an active
// segment.
// rewind()函数将s.r置s.b。即当前segment需要重新遍历。需要注意边界值s.b == -1的时候
func (s *source) rewind() {
	// ok to verify precondition - rewind is rarely called
	if s.b < 0 {
		panic("no active segment")
	}
	s.col -= uint(s.r - s.b) // 这里可以看出倒带不能跨行
	s.r = s.b
	s.nextch()
}

// source.go的核心函数。
func (s *source) nextch() {
redo:
	// 行列信息更新是公共部分，移动出来复用。其实是处理上一次
	s.col += uint(s.chw) // 首先移动上一次读取chr时候未更新的列信息。开始边界时s.chw为0，col不变
	if s.ch == '\n' { // 新行更新行列信息，行只有换行符的时候更新
		s.line++
		s.col = 0
	}
	// fast common case: at least one ASCII character
	// 更新ch 如果是ASCII集，更新r,chw
	if s.ch = rune(s.buf[s.r]); s.ch < sentinel { // 可以对照着source.buf初始化的时候，buf[0] = sentinel的小细节
		s.r++
		s.chw = 1
		if s.ch == 0 {
			s.error("invalid NUL character")
			goto redo
		}
		return
	}
	// slower general case: add more bytes to buffer if we don't have a full rune
	// 如果不是ASCII字符集/buf中读取完了
	// 且utf8编码的char一个最多有utf8.UTFMax个字节。所以 < utf8.UTFMax时需要填充
	// 当文件结尾时，会s.ioerr != nil。所以要有这个条件，避免死循环
	for s.e-s.r < utf8.UTFMax && !utf8.FullRune(s.buf[s.r:s.e]) && s.ioerr == nil {
		s.fill()
	}
	// EOF
	// 处理因读取完而产生的出错。ch,chw置初始值，返回
	if s.r == s.e {
		if s.ioerr != io.EOF {
			// ensure we never start with a '/' (e.g., rooted path) in the error message
			s.error("I/O error: " + s.ioerr.Error())
			s.ioerr = nil
		}
		s.ch = -1
		s.chw = 0
		return
	}
	// 获取UTF-8编码的字符，更新ch,r,chw。因为上面的ch是读取的UTF-8编码字符的最高位字符
	// 其实第一次读取源码到buf也是靠这个读所有字符，包括ASCII
	s.ch, s.chw = utf8.DecodeRune(s.buf[s.r:s.e])
	s.r += s.chw
	// 获取到错误编码的字符报错并忽略读下一个字符
	if s.ch == utf8.RuneError && s.chw == 1 {
		s.error("invalid UTF-8 encoding")
		goto redo
	}
	// BOM's are only allowed as the first character in a file
	// BOM解释：https://zh.wikipedia.org/wiki/%E4%BD%8D%E5%85%83%E7%B5%84%E9%A0%86%E5%BA%8F%E8%A8%98%E8%99%9F
	// 是BOM且在第一个就跳过读取下一个字符，不在第一个字符就报错并读下一个字符
	const BOM = 0xfeff
	if s.ch == BOM {
		if s.line > 0 || s.col > 0 {
			s.error("invalid BOM in the middle of the file")
		}
		goto redo
	}
}

// fill reads more source bytes into s.buf.
// It returns with at least one more byte in the buffer, or with s.ioerr != nil.
func (s *source) fill() {
	// determine content to preserve
	b := s.r // 开始边界
	if s.b >= 0 {
		b = s.b
		s.b = 0 // after buffer has grown or content has been moved down
	}
	content := s.buf[b:s.e]

	// grow buffer or move content down 扩容后拷贝原内容。当然，前提是有内容
	if len(content)*2 > len(s.buf) { // 当content的2倍长度大于缓存区长度时，扩容缓存区
		s.buf = make([]byte, nextSize(len(s.buf)))
		copy(s.buf, content)
	} else if b > 0 { // 避免了开始边界
		copy(s.buf, content)
	}
	s.r -= b // 这2行是归档。因为当s.b >= 0或者是开始边界时b前面的我们都不需要了
	s.e -= b

	// read more data: try a limited number of times
	// 有限的读取。并更新e的位置
	for i := 0; i < 10; i++ {
		var n int                                         // 新读取的byte数
		n, s.ioerr = s.in.Read(s.buf[s.e : len(s.buf)-1]) // -1 to leave space for sentinel
		if n < 0 {
			panic("negative read") // incorrect underlying io.Reader implementation
		}
		if n > 0 || s.ioerr != nil {
			s.e += n
			s.buf[s.e] = sentinel
			return
		}
		// n == 0
	}
	// 10次循环后还未成功，记录错误，放弃
	s.buf[s.e] = sentinel
	s.ioerr = io.ErrNoProgress
}

// nextSize returns the next bigger size for a buffer of a given size.
func nextSize(size int) int {
	const min = 4 << 10 // 4K: minimum buffer size
	const max = 1 << 20 // 1M: maximum buffer size which is still doubled
	if size < min {
		return min
	}
	if size <= max {
		return size << 1
	}
	return size + max
}
